import {Component, OnInit} from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { ServicoProvider } from "../../providers/servico";
import { Storage } from "@ionic/storage";

@Component({
  selector: 'page-servico',
  templateUrl: 'servico.html',
})
export class servico implements OnInit {

  public usuario: any = {
      uid: null,
      email: null,
      data_nascimento: null,
      nome: null
  };

  public servico: any = {
      uid: null,
      categoria: null,
      localidade: null,
      descricao: null,
      diferencial: null
  }

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public toastCtrl: ToastController,
      private storage: Storage,
      private servicoProvider: ServicoProvider,
      private loadingCtrl: LoadingController
  ) {

  }

  async ngOnInit() {
      this.usuario = await this.storage.get('usuario');
      const servicoFirebase = (await this.servicoProvider.getServico(this.usuario.uid)).data();
      if (servicoFirebase) {
        this.servico = servicoFirebase;
      }
  }

  temServicoCadastrado() {
      return !! this.servico.uid;
  }

  presentToast() {
    const toast = this.toastCtrl.create({
      message: 'Serviço cadastrado com sucesso!',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  openDetail(){
    this.navCtrl.parent.select(3);
    this.navCtrl.pop();
    this.presentToast();
  }

  async cadastrar() {
    const load = this.loadingCtrl.create();
    load.present();
    try {
      const data = {
        ... this.servico,
        uid: this.usuario.uid,
      };

      await this.servicoProvider.postServico(data);
      this.openDetail();
    } catch (error) {
        const toast = this.toastCtrl.create({
            message: 'Erro ao cadastrar serviço. Tente novamente mais tarde',
            duration: 3000,
            position: 'top'
        });
        toast.present();
    } finally {
        load.dismiss();
    }
  }

  private async alterar() {
      const load = this.loadingCtrl.create();
      load.present();
      try {
          await this.servicoProvider.updateService(
              this.usuario.uid,
              this.servico
          )
      } catch (error) {
          console.log(error);
          const toast = this.toastCtrl.create({
              message: 'Erro ao alterar serviço. Tente novamente mais tarde',
              duration: 3000,
              position: 'top'
          });
          toast.present();
      } finally {
          load.dismiss();
      }
  }

  async salvar() {
      const load = this.loadingCtrl.create();
      load.present();
      try {
          if (! this.temServicoCadastrado()) {
              await this.cadastrar()
          } else {
              await this.alterar()
          }
      } catch (e) {
          const toast = this.toastCtrl.create({
              message: 'Erro ao alterar serviço. Tente novamente mais tarde',
              duration: 3000,
              position: 'top'
          });
          toast.present();
      } finally {
          load.dismiss();
      }
  }
}
