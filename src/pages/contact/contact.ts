import {Component, OnInit} from '@angular/core';
import {NavController, AlertController, LoadingController} from 'ionic-angular';
import {servico} from '../servico/servico';
import {GerenciarServicoPage} from '../gerenciar-servico/gerenciar-servico';
import {Usuario, UsuarioProvider} from "../../providers/usuario";
import {ServicoProvider} from "../../providers/servico";
import {UsuarioStorage } from "../../providers/usuario-storage";
import { from } from 'rxjs';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage implements OnInit {
  
  public chats:any;
  public usuario: Usuario;

  constructor(
      public navCtrl: NavController,
      public alertCtrl: AlertController,
      private usuarioProvider: UsuarioProvider,
      private servicoProvider: ServicoProvider,
      private loadingCtrl: LoadingController,
      private usuarioStorage: UsuarioStorage,
  ) {}

  async ngOnInit() {
      const usuario = {
          ... await this.usuarioStorage.getUsuarioLogado(),
          face: 'assets/imgs/user1.png'
      };

      this.usuario = usuario;
      this.chats = [usuario]
  }

  openDetail(nome,email,face,nascimento){
    this.navCtrl.push(servico,{
      nome: nome,
      email: email,
      face: face,
      nascimento: nascimento,
    });
   }

   openServicoDetail(nome){
        this.navCtrl.push(GerenciarServicoPage,{
          nome : nome,
        });
   }

   showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Ops!',
      subTitle: 'A função "Promover Serviço" será implementada em versões futuras do App.',
      buttons: ['Continuar']
    });
    alert.present();
  }

  async alterarUsuario() {
      const load = this.loadingCtrl.create();
      load.present();
      try {
          await this.usuarioProvider.update(
              this.usuario.uid,
              this.usuario
          )

          this.usuarioStorage.updateUsuario(this.usuario)
      } catch (e) {
         console.log(e);
      } finally {
          load.dismissAll();
      }
  }
}
