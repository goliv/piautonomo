import { Component, OnInit} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-homedetail',
  templateUrl: 'homedetail.html',
})
export class HomeDetailPage implements OnInit{
  public nome:any;
  public funcao:any;
  public foto:any;
  public disconto:any;
  public preco: any;
  public descricao:any;
  public telefone:any;
  public localidade:any;
  public diferencial1:any;
  public diferencial2:any;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.foto=navParams.get('foto');
    this.nome=navParams.get('nome');
    this.funcao=navParams.get('funcao');
    this.preco= navParams.get('preco');
    this.disconto=navParams.get('disconto');    
    this.descricao=navParams.get('descricao');
    this.telefone=navParams.get('telefone');
    this.localidade=navParams.get('localidade');
    this.diferencial1=navParams.get('diferencial1');
    this.diferencial2=navParams.get('diferencial2');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomedetailPage',this.preco);
  }
  async ngOnInit() {}
}
