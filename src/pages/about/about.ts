import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import { DataProvider } from '../../providers/dataprovider/dataprovider';
import { HomeDetailPage } from '../homedetail/homedetail';
import {ServicoProvider} from "../../providers/servico";
import {UsuarioProvider} from "../../providers/usuario";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage implements OnInit{
  public chats:any[] = [];
  public items:any;
  public layouticon:any;
  public layout:any;

  constructor(
      public navCtrl: NavController,
      public dataProvider : DataProvider,
      private servicoProvider: ServicoProvider,
      private usuarioProvider: UsuarioProvider
  ) {
      // this.chats= dataProvider.getall();
      // console.log('this.chats',this.chats)
      this.layouticon='list';
      this.layout="list-view";
      this.initializeItems();

  }
  openDetail(foto,nome,funcao,descricao,telefone,localidade,diferencial){
    this.navCtrl.push(HomeDetailPage,{
      foto : foto,
      nome : nome,
      funcao:funcao,
      descricao:descricao,
      telefone:telefone,
      localidade:localidade,
      diferencial1:diferencial,
    });
   }

    async ngOnInit() {
      const servicos = await this.servicoProvider.listar();

      await servicos.forEach(async queryDocument => {
        this.items = [];
        queryDocument.forEach(async servico => {
          const servicoObject = Object.assign( {uid: null}, servico)
          const usuario = await this.usuarioProvider.getUser(servicoObject.uid || null);
          this.items.push({
            usuario: {... usuario.data(), face: 'assets/imgs/user1.png'},
            servico
          })
        });
      });
    }

  changeicon(){
    if(this.layouticon == 'grid'){
      this.layouticon='list';
 	this.layout="list-view";
    }else{
      this.layouticon='grid';
	 this.layout="grid-view";
    }
  }
  initializeItems(){
    this.items=this.chats;
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.funcao.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

}
