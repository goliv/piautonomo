import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import {DataProvider} from '../../providers/dataprovider/dataprovider';
import {HomeDetailPage} from '../homedetail/homedetail';
import {ServicoProvider} from "../../providers/servico";
import {UsuarioProvider} from "../../providers/usuario";
import {servico} from "../servico/servico";
import {query} from "@angular/animations";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})

export class HomePage implements OnInit {
  public chats: any[] = [];
  public layouticon:any;
  public layout:any;
  delete(chip: Element) {
    chip.remove();
  }

  constructor(
      public navCtrl: NavController,
      public dataProvider : DataProvider,
      private servicoProvider: ServicoProvider,
      private usuarioProvider: UsuarioProvider,
  ) {
      this.servicoProvider.listar()


      console.log('this.chats',this.chats)
      this.layouticon='list';
      this.layout="grid-view";

  }

  async ngOnInit() {

    const servicos = await this.servicoProvider.listar();

    await servicos.forEach(async queryDocument => {
      this.chats = [];
      queryDocument.forEach(async servico => {
        const servicoObject = Object.assign({uid: null}, servico);
        console.log('s ...', servico, servicoObject);
        const usuario = await this.usuarioProvider.getUser(servicoObject.uid);
        this.chats.push({
          usuario: {... usuario.data(), face: 'assets/imgs/user1.png'},
          servico
        })
      });
    });
  }

  openDetail(foto,nome,funcao,descricao,telefone,localidade,diferencial){
    this.navCtrl.push(HomeDetailPage,{
      foto : foto,
      nome : nome,
      funcao:funcao,
      descricao:descricao,
      telefone:telefone,
      localidade:localidade,
      diferencial1:diferencial,
    });
   }
  changeicon(){
	console.log('change',this.layouticon)
    if(this.layouticon == 'grid'){
      this.layouticon='list';
 	this.layout="list-view";
    }else{
      this.layouticon='grid';
	 this.layout="grid-view";
    }
  }

}

