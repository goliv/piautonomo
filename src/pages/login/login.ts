import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/AuthProvider";
import { UsuarioProvider } from "../../providers/usuario";
import { Storage } from "@ionic/storage";
import { TabsPage } from "../tabs/tabs";


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public login: boolean = true;
  public register: boolean = false;
  public title: string = "Login";
  public loginForm = {
      email: null,
      senha: null
  };

  public registroForm = {
      nome: null,
      email: null,
      data_nascimento: null,
      senha: null,
      telefone: null,
  }

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private authProvider: AuthProvider,
      private usuarioProvider: UsuarioProvider,
      private storage: Storage,
      private loadingCtrl: LoadingController,
  ) {
  }

  ionViewDidLoad() {

  }

  exibirRegistrar() {
    this.title = "Registrar";
    this.login = false;
    this.register = true;
  }

  exibirLogin() {
    this.title = "Login";
    this.login = true;
    this.register = false;
  }

  async logar() {
    console.log("logar ....");
    const load = this.loadingCtrl.create();
    load.present();

    try {
      const responseLogin = await this.authProvider.login(this.loginForm);
      const usuario = await this.usuarioProvider.getUser(responseLogin.user.uid);
      console.log('usuario login', usuario.data(), responseLogin.user.uid);
      this.storage.set('usuario', usuario.data());
      console.log(this.storage.get('usuario'));
      this.navCtrl.setRoot(TabsPage);
    } catch (e) {
      console.log(e);
    } finally {
      load.dismiss();
    }
  }

  async registrar() {
    const load = this.loadingCtrl.create();
    load.present();
    try {
      const response = await this.authProvider.register(this.registroForm);
      const data = {
        uid: response.user.uid,
        ... this.registroForm
      };
      await this.usuarioProvider.postUser(data);
      this.storage.set('usuario', data);
      this.navCtrl.setRoot(TabsPage);
    } catch (error) {
      console.log(error);
    } finally {
      load.dismiss()
    }
  }
}
