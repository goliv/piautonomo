import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GerenciarServicoPage } from './gerenciar-servico';

@NgModule({
  declarations: [
    GerenciarServicoPage,
  ],
  imports: [
    IonicPageModule.forChild(GerenciarServicoPage),
  ],
})
export class GerenciarServicoPageModule {}
