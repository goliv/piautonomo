import { Component, OnInit} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import { DataProvider } from '../../providers/dataprovider/dataprovider';
import {ServicoProvider} from "../../providers/servico";
import {UsuarioProvider} from "../../providers/usuario";
import {UsuarioStorage } from "../../providers/usuario-storage";
import { Storage } from "@ionic/storage";
import {TabsPage} from "../tabs/tabs";


@IonicPage()
@Component({
  selector: 'page-gerenciar-servico',
  templateUrl: 'gerenciar-servico.html',
})
export class GerenciarServicoPage implements OnInit{
  public usuario: any = {
    uid: null,
    email: null,
    data_nascimento: null,
    nome: null
};

public servico: any = {
  uid: null,
  categoria: null,
  localidade: null,
  descricao: null,
  diferencial: null
}

  public chats:any[] = [];
  public items:any={};
  public nome:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public dataProvider : DataProvider,
    private servicoProvider: ServicoProvider,
    private usuarioProvider: UsuarioProvider,
    private usuarioStorage: UsuarioStorage,
    private storage: Storage,
    private loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    ) 
    {
      this.initializeItems();
      this.nome=navParams.get('nome');
  }

  initializeItems(){
    this.items=this.chats;
  }

  async deletarServico() {
    const load = this.loadingCtrl.create();
    load.present();
    try {
      await this.servicoProvider.excluirServico(this.usuario['uid'].toString());
        const toast = this.toastCtrl.create({
            message: 'Serviço excluido com sucesso.',
            duration: 3000,
            position: 'top'
        });
        toast.present();
      this.navCtrl.setRoot(TabsPage);
    } catch (error) {
        const toast = this.toastCtrl.create({
            message: 'Erro ao excluir serviço. Tente novamente mais tarde',
            duration: 3000,
            position: 'top'
        });
        toast.present();
    } finally {
        load.dismiss();
    }
  }

  async ngOnInit() {
    this.usuario = await this.storage.get('usuario');
      const servicoFirebase = (await this.servicoProvider.getServico(this.usuario.uid)).data();
      if (servicoFirebase) {
        this.servico = servicoFirebase;
      }
     }

}
