import { Storage } from "@ionic/storage";
import {Usuario} from "./usuario";
import {Injectable} from "@angular/core";

@Injectable()
export class UsuarioStorage {

    private chaveUsuario: string = 'usuario';

    public constructor(
       private storage: Storage
    ) {}

    getUsuarioLogado() {
        return this.storage.get(this.chaveUsuario);
    }

    updateUsuario(usuario: Usuario) {
        return this.storage.set(this.chaveUsuario, usuario);
    }
}