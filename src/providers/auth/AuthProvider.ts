import { Injectable } from "@angular/core";
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class AuthProvider {

    constructor(
        private afAuth: AngularFireAuth
    ) {
    }

    // Criar usuário
    register = (data) => this.afAuth.auth.createUserAndRetrieveDataWithEmailAndPassword(data.email, data.senha);

    // Login usuário
    login = (data) => this.afAuth.auth.signInWithEmailAndPassword(data.email, data.senha);
}