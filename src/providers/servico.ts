import { AngularFirestore } from "angularfire2/firestore";
import { Injectable } from "@angular/core";
import {Usuario, UsuarioProvider} from "./usuario";
import { UsuarioStorage } from "./usuario-storage";

export interface Servico {
    uid: string,
    categoria: string,
    localidade: string,
    descricao: string,
    diferencial: string
}

export interface UsuarioServico extends Usuario {
    servicos: Servico
}

@Injectable()
export class ServicoProvider {
    private chave: string = 'servicos';

    constructor(
        private angularFirestore: AngularFirestore,
        private usuarioProvider: UsuarioProvider,
        private usuarioStorage: UsuarioStorage,
    ) {}

    // Criar servico no firestore
    postServico = (data: Servico) => {
        return this.angularFirestore.collection(this.chave).doc(data.uid).set(data);
    }

    getServico =  uid => this.angularFirestore.collection(this.chave)
        .doc(uid)
        .get()
        .toPromise()

    updateService = async (uid, servico: Servico) => {

        const dadosServicoAtual = (await this.getServico(uid)).data();
        const servicoUpdate = {
            ... dadosServicoAtual,
            ... servico
        };

        return this.angularFirestore.collection(this.chave)
            .doc(uid)
            .update(servicoUpdate)
    }

    listar = async () => {
        return await this.angularFirestore.collection(this.chave)
            .valueChanges()
    }
    excluirServico(uid){
        return this.angularFirestore.collection(this.chave).doc(uid).delete();
    }
}