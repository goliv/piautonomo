import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class DataProvider {

public alldata:any=[];
public destaque:any=[];


  constructor() {
    this.destaque=[{
      id: 0,
            nome: 'Robson Martins',
            funcao: 'Carpinteiro',
            foto: 'assets/imgs/user1.png',
            preco:'50',
            disconto:'30%',
            descricao:'Descrição do serviço',
            telefone:'(31) 3333-0927',
            localidade:'Novo Riacho',
            diferencial1:'loja fisica',
            diferencial2:'manutencao',
          }, {
            id: 1,
            nome: 'Alice Vieira',
            funcao: 'Função',
            foto: 'assets/imgs/user2.png',
            preco:'80',
            disconto:'10%',
            descricao:'',
            telefone:'',
            localidade:'Novo Riacho',
            diferencial1:'Teste',
            diferencial2:'Teste 2'
          }, {
            id: 2,
            nome: 'Marcela Santos',
            funcao: 'Função',
            foto: 'assets/imgs/user3.png',
            preco:'120',
            disconto:'20%',
            descricao:'',
            telefone:'',
            localidade:'Novo Riacho',
            diferencial1:'Teste',
            diferencial2:'Teste 2'
          }, {
            id: 3,
            nome: 'Guilherme Soares',
            funcao: 'Função',
            foto: 'assets/imgs/user1.png',
            preco:'30',
            disconto:'15%',
            descricao:'',
            telefone:'',
            localidade:'Novo Riacho',
            diferencial1:'Teste',
            diferencial2:'Teste 2'
    }];
        this.alldata = [{
            id: 0,
            nome: 'Robson Martins',
            funcao: 'Carpinteiro',
            foto: 'assets/imgs/user1.png',
            preco:'50',
            disconto:'30%',
            descricao:'Descrição do serviço',
            telefone:'(31) 3333-0927',
            localidade:'Novo Riacho',
            diferencial1:'loja fisica',
            diferencial2:'manutencao',
          }, {
            id: 1,
            nome: 'Alice Vieira',
            funcao: 'Função',
            foto: 'assets/imgs/user2.png',
            preco:'80',
            disconto:'10%',
            descricao:'',
            telefone:'',
            localidade:'Novo Riacho',
            diferencial1:'Teste',
            diferencial2:'Teste 2'
          }, {
            id: 2,
            nome: 'Marcela Santos',
            funcao: 'Função',
            foto: 'assets/imgs/user3.png',
            preco:'120',
            disconto:'20%',
            descricao:'',
            telefone:'',
            localidade:'Novo Riacho',
            diferencial1:'Teste',
            diferencial2:'Teste 2'
          }, {
            id: 3,
            nome: 'Guilherme Soares',
            funcao: 'Função',
            foto: 'assets/imgs/user1.png',
            preco:'30',
            disconto:'15%',
            descricao:'',
            telefone:'',
            localidade:'Novo Riacho',
            diferencial1:'Teste',
            diferencial2:'Teste 2'
          }, {
            id: 4,
            nome: 'Daniele Silva',
            funcao: 'Função',
            foto: 'assets/imgs/user2.png',
            preco:'210',
            disconto:'25%',
            descricao:'',
            telefone:'',
            localidade:'Novo Riacho',
            diferencial1:'Teste',
            diferencial2:'Teste 2'
          }];

  }
  getall(){
    return this.alldata;
  }
  getDestaque(){    
    return this.destaque;
  }

}
