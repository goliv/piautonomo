import { AngularFirestore } from "angularfire2/firestore";
import { Injectable } from "@angular/core";

export interface Usuario {
    uid: string,
    email: string,
    data_nascimento: string,
    nome: string,
    telefone: string
}

@Injectable()
export class UsuarioProvider {

    constructor(private angularFirestore: AngularFirestore) {}

    // Criar usuário no firestore
    postUser = (data: Usuario) => this.angularFirestore.collection('usuarios').doc(data.uid).set(data);

    getUser =  uid => this.angularFirestore.collection('usuarios')
        .doc(uid)
        .get()
        .toPromise()

    update = async (uid, usuario: Usuario) => {
        const dadosUsuarioAtual = await this.getUser(uid);
        const usuarioUpdate = {
            ... dadosUsuarioAtual.data(),
            ... usuario
        }

        return this.angularFirestore.collection('usuarios')
            .doc(uid)
            .update(usuarioUpdate)
    }
}