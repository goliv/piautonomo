import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import {GerenciarServicoPage} from '../pages/gerenciar-servico/gerenciar-servico';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomeDetailPage } from '../pages/homedetail/homedetail';
import { servico } from '../pages/servico/servico';
import { LayoutSwitcherDirective } from '../directives/layout-switcher/layout-switcher';
import { ElasticHeaderDirective } from '../directives/elastic-header/elastic-header';
import { AngularFireModule } from 'angularfire2';


import FirebaseConfig from '../providers/configs/firebase-config';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule} from 'angularfire2/auth';


// Plugin
import { IonicStorageModule } from "@ionic/storage";

// PROVIDERS
import { DataProvider } from '../providers/dataprovider/dataprovider';
import { DataProviderProfile } from '../providers/dataprovider/profile';
import { AuthProvider } from '../providers/auth/AuthProvider';
import { UsuarioProvider } from "../providers/usuario";
import { ServicoProvider } from "../providers/servico";
import { UsuarioStorage } from "../providers/usuario-storage";

// PAGES
import { LoginPageModule } from "../pages/login/login.module";

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    HomeDetailPage,
    servico,
    GerenciarServicoPage,
    TabsPage,
    LayoutSwitcherDirective,
    ElasticHeaderDirective,
  ],
  imports: [
    LoginPageModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(FirebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot(),
  ],
  exports:[GerenciarServicoPage],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    GerenciarServicoPage,
    ContactPage,
    HomePage,
    HomeDetailPage,
    servico,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider,
    DataProviderProfile,
    AuthProvider,
    UsuarioProvider,
    ServicoProvider,
    UsuarioStorage
  ]
})
export class AppModule {}

