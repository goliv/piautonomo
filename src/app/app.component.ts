import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from "@ionic/storage";
import { UsuarioStorage } from "../providers/usuario-storage";


import { TabsPage } from '../pages/tabs/tabs';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = 'LoginPage';

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private usuarioStorage: UsuarioStorage) {
    platform.ready().then(() => {

      this.usuarioStorage.getUsuarioLogado()
          .then(usuario => {
            this.rootPage = (usuario) ? TabsPage : 'LoginPage'
          })

        statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
