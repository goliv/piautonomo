webpackJsonp([1],{

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(287);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__about_about__["a" /* AboutPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/tabs/tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="Destaques" tabIcon="star"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Serviços" tabIcon="briefcase"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Perfil" tabIcon="person"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


var HomeDetailPage = /** @class */ (function () {
    function HomeDetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.foto = navParams.get('foto');
        this.nome = navParams.get('nome');
        this.funcao = navParams.get('funcao');
        this.preco = navParams.get('preco');
        this.disconto = navParams.get('disconto');
        this.descricao = navParams.get('descricao');
        this.telefone = navParams.get('telefone');
        this.localidade = navParams.get('localidade');
        this.diferencial1 = navParams.get('diferencial1');
        this.diferencial2 = navParams.get('diferencial2');
    }
    HomeDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomedetailPage', this.preco);
    };
    HomeDetailPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/];
        }); });
    };
    HomeDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-homedetail',template:/*ion-inline-start:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/homedetail/homedetail.html"*/'<ion-header>\n\n  <div class="main-header">\n    <ion-navbar color="danger">\n      <ion-title>{{nome}}</ion-title>\n    </ion-navbar>\n  </div>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="content-detail-wrap">\n    <img  src="{{foto}}">\n    <div class="sub-details">{{funcao}}</div>\n    <!--<div class="sub-price"><strong>R$ {{preco}}</strong></div>-->\n    <div class="sub-discount">Tel: {{telefone}}</div>\n    <div class="sub-discount">Localidade: {{localidade}}</div>\n    <div class="sub-discount">{{descricao}}</div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/homedetail/homedetail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], HomeDetailPage);
    return HomeDetailPage;
}());

//# sourceMappingURL=homedetail.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GerenciarServicoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dataprovider_dataprovider__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_servico__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_usuario__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_usuario_storage__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__tabs_tabs__ = __webpack_require__(108);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var GerenciarServicoPage = /** @class */ (function () {
    function GerenciarServicoPage(navCtrl, navParams, dataProvider, servicoProvider, usuarioProvider, usuarioStorage, storage, loadingCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataProvider = dataProvider;
        this.servicoProvider = servicoProvider;
        this.usuarioProvider = usuarioProvider;
        this.usuarioStorage = usuarioStorage;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.usuario = {
            uid: null,
            email: null,
            data_nascimento: null,
            nome: null
        };
        this.servico = {
            uid: null,
            categoria: null,
            localidade: null,
            descricao: null,
            diferencial: null
        };
        this.chats = [];
        this.items = {};
        this.initializeItems();
        this.nome = navParams.get('nome');
    }
    GerenciarServicoPage.prototype.initializeItems = function () {
        this.items = this.chats;
    };
    GerenciarServicoPage.prototype.deletarServico = function () {
        return __awaiter(this, void 0, void 0, function () {
            var load, toast, error_1, toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        load = this.loadingCtrl.create();
                        load.present();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        return [4 /*yield*/, this.servicoProvider.excluirServico(this.usuario['uid'].toString())];
                    case 2:
                        _a.sent();
                        toast = this.toastCtrl.create({
                            message: 'Serviço excluido com sucesso.',
                            duration: 3000,
                            position: 'top'
                        });
                        toast.present();
                        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__tabs_tabs__["a" /* TabsPage */]);
                        return [3 /*break*/, 5];
                    case 3:
                        error_1 = _a.sent();
                        toast = this.toastCtrl.create({
                            message: 'Erro ao excluir serviço. Tente novamente mais tarde',
                            duration: 3000,
                            position: 'top'
                        });
                        toast.present();
                        return [3 /*break*/, 5];
                    case 4:
                        load.dismiss();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    GerenciarServicoPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, servicoFirebase;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storage.get('usuario')];
                    case 1:
                        _a.usuario = _b.sent();
                        return [4 /*yield*/, this.servicoProvider.getServico(this.usuario.uid)];
                    case 2:
                        servicoFirebase = (_b.sent()).data();
                        if (servicoFirebase) {
                            this.servico = servicoFirebase;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    GerenciarServicoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-gerenciar-servico',template:/*ion-inline-start:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/gerenciar-servico/gerenciar-servico.html"*/'<!--\n  Generated template for the GerenciarServicoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <div class="main-header">\n      <ion-navbar color="danger">\n        <ion-title>Gerenciar Serviço</ion-title>\n        </ion-navbar>\n        </div>\n\n</ion-header>\n<ion-content padding>\n\n  <div class="card">\n      <h1>{{servico.categoria}}</h1>\n      <p>{{servico.descricao}}</p>\n      <p><button ion-button full class="promoverServico" (click)="deletarServico()">Excluir Serviço</button></p>\n    </div> \n\n\n\n\n\n\n</ion-content>\n'/*ion-inline-end:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/gerenciar-servico/gerenciar-servico.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_dataprovider_dataprovider__["a" /* DataProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_servico__["a" /* ServicoProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_usuario__["a" /* UsuarioProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_usuario_storage__["a" /* UsuarioStorage */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */]])
    ], GerenciarServicoPage);
    return GerenciarServicoPage;
}());

//# sourceMappingURL=gerenciar-servico.js.map

/***/ }),

/***/ 227:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 227;

/***/ }),

/***/ 269:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/gerenciar-servico/gerenciar-servico.module": [
		581,
		0
	],
	"../pages/login/login.module": [
		288
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 269;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dataprovider_dataprovider__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__homedetail_homedetail__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_servico__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_usuario__ = __webpack_require__(52);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl, dataProvider, servicoProvider, usuarioProvider) {
        this.navCtrl = navCtrl;
        this.dataProvider = dataProvider;
        this.servicoProvider = servicoProvider;
        this.usuarioProvider = usuarioProvider;
        this.chats = [];
        // this.chats= dataProvider.getall();
        // console.log('this.chats',this.chats)
        this.layouticon = 'list';
        this.layout = "list-view";
        this.initializeItems();
    }
    AboutPage.prototype.openDetail = function (foto, nome, funcao, descricao, telefone, localidade, diferencial) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__homedetail_homedetail__["a" /* HomeDetailPage */], {
            foto: foto,
            nome: nome,
            funcao: funcao,
            descricao: descricao,
            telefone: telefone,
            localidade: localidade,
            diferencial1: diferencial,
        });
    };
    AboutPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var servicos;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.servicoProvider.listar()];
                    case 1:
                        servicos = _a.sent();
                        return [4 /*yield*/, servicos.forEach(function (queryDocument) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    this.items = [];
                                    queryDocument.forEach(function (servico) { return __awaiter(_this, void 0, void 0, function () {
                                        var servicoObject, usuario;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    servicoObject = Object.assign({ uid: null }, servico);
                                                    return [4 /*yield*/, this.usuarioProvider.getUser(servicoObject.uid || null)];
                                                case 1:
                                                    usuario = _a.sent();
                                                    this.items.push({
                                                        usuario: __assign({}, usuario.data(), { face: 'assets/imgs/user1.png' }),
                                                        servico: servico
                                                    });
                                                    return [2 /*return*/];
                                            }
                                        });
                                    }); });
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AboutPage.prototype.changeicon = function () {
        if (this.layouticon == 'grid') {
            this.layouticon = 'list';
            this.layout = "list-view";
        }
        else {
            this.layouticon = 'grid';
            this.layout = "grid-view";
        }
    };
    AboutPage.prototype.initializeItems = function () {
        this.items = this.chats;
    };
    AboutPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.funcao.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/about/about.html"*/'<ion-header>\n    <div class="main-header">\n      <ion-navbar color="danger">\n        <ion-title *ngIf="!isSearchbarOpened">Serviços</ion-title>\n         <ion-searchbar (ionInput)="getItems($event)" *ngIf="isSearchbarOpened" showCancelButton="true" (ionCancel)="isSearchbarOpened=false"></ion-searchbar>\n          <ion-buttons end>\n            <button ion-button icon-only *ngIf="!isSearchbarOpened" (click)="isSearchbarOpened=true" disabled>\n              <ion-icon name="search"></ion-icon>\n            </button>\n         </ion-buttons>\n      </ion-navbar>\n    </div>\n  </ion-header>\n\n<ion-content>\n    <div class="chat-list-wrap {{layout}}">\n        <ion-list>\n          <ion-item  *ngFor="let item of items" (click)="openDetail(item.usuario.face,item.usuario.nome,item.servico.categoria,item.servico.descricao,item.usuario.telefone,item.servico.localidade,item.servico.diferencial)">\n            <img  src="{{item.usuario.face}}">\n            <div class="chat-list-title">{{item.usuario.nome}}</div>\n            <div class="chat-list-sub-title">{{item.servico.categoria}}<div class="chat-list-localidade"><ion-icon ios="ios-pin" md="md-pin"></ion-icon> {{item.servico.localidade}}</div></div>\n            \n            <div class="chat-list-item">{{item.servico.diferencial}}</div>\n          </ion-item>\n        </ion-list>\n      </div>\n\n</ion-content>\n'/*ion-inline-end:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/about/about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_dataprovider_dataprovider__["a" /* DataProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_servico__["a" /* ServicoProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_usuario__["a" /* UsuarioProvider */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__servico_servico__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__gerenciar_servico_gerenciar_servico__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_usuario__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_servico__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_usuario_storage__ = __webpack_require__(83);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl, alertCtrl, usuarioProvider, servicoProvider, loadingCtrl, usuarioStorage) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.usuarioProvider = usuarioProvider;
        this.servicoProvider = servicoProvider;
        this.loadingCtrl = loadingCtrl;
        this.usuarioStorage = usuarioStorage;
    }
    ContactPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var usuario, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = [{}];
                        return [4 /*yield*/, this.usuarioStorage.getUsuarioLogado()];
                    case 1:
                        usuario = __assign.apply(void 0, _a.concat([_b.sent(), { face: 'assets/imgs/user1.png' }]));
                        this.usuario = usuario;
                        this.chats = [usuario];
                        return [2 /*return*/];
                }
            });
        });
    };
    ContactPage.prototype.openDetail = function (nome, email, face, nascimento) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__servico_servico__["a" /* servico */], {
            nome: nome,
            email: email,
            face: face,
            nascimento: nascimento,
        });
    };
    ContactPage.prototype.openServicoDetail = function (nome) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__gerenciar_servico_gerenciar_servico__["a" /* GerenciarServicoPage */], {
            nome: nome,
        });
    };
    ContactPage.prototype.showAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Ops!',
            subTitle: 'A função "Promover Serviço" será implementada em versões futuras do App.',
            buttons: ['Continuar']
        });
        alert.present();
    };
    ContactPage.prototype.alterarUsuario = function () {
        return __awaiter(this, void 0, void 0, function () {
            var load, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        load = this.loadingCtrl.create();
                        load.present();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        return [4 /*yield*/, this.usuarioProvider.update(this.usuario.uid, this.usuario)];
                    case 2:
                        _a.sent();
                        this.usuarioStorage.updateUsuario(this.usuario);
                        return [3 /*break*/, 5];
                    case 3:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [3 /*break*/, 5];
                    case 4:
                        load.dismissAll();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/contact/contact.html"*/'<ion-header>\n  <div class="main-header">\n    <ion-navbar color="danger">\n      <ion-title>Perfil</ion-title>\n        <ion-buttons end>\n      </ion-buttons>\n    </ion-navbar>\n  </div>\n</ion-header>\n\n<ion-content padding>\n  <ion-item *ngFor="let chat of chats">\n    <img src="{{chat.face}}" class="chat-face">\n  </ion-item>\n\n  <ion-item *ngFor="let chat of chats" >\n      <ion-label floating>Nome</ion-label>\n      <ion-input type="text" [(ngModel)]="usuario.nome"></ion-input>\n    </ion-item>\n\n    <ion-item *ngFor="let chat of chats">\n      <ion-label floating>Email</ion-label>\n      <ion-input type="text" [(ngModel)]="usuario.email"></ion-input>\n    </ion-item>  \n        \n      <ion-item *ngFor="let chat of chats">\n          <ion-label floating>Nascimento</ion-label>\n          <ion-input type="text" [(ngModel)]="usuario.data_nascimento"></ion-input>\n      </ion-item>\n      <br><br><br>\n      <button ion-button full class="button button-wp-secondary" *ngFor="let chat of chats" (click)="alterarUsuario()">Alterar Informações</button>\n      <button ion-button full class="addServico" *ngFor="let chat of chats" (click)="openDetail(chat.nome,chat.email,chat.face,chat.nascimento)">Cadastrar Serviço</button>\n      <button ion-button full class="gerenciarServico" *ngFor="let chat of chats" (click)="openServicoDetail(chat.nome)">Gerenciar Serviço</button>\n      <button ion-button full class="promoverServico" (click)="showAlert()">Promover Serviço</button>\n</ion-content>\n'/*ion-inline-end:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/contact/contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_usuario__["a" /* UsuarioProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_servico__["a" /* ServicoProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_usuario_storage__["a" /* UsuarioStorage */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return servico; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_servico__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(84);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var servico = /** @class */ (function () {
    function servico(navCtrl, navParams, toastCtrl, storage, servicoProvider, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.servicoProvider = servicoProvider;
        this.loadingCtrl = loadingCtrl;
        this.usuario = {
            uid: null,
            email: null,
            data_nascimento: null,
            nome: null
        };
        this.servico = {
            uid: null,
            categoria: null,
            localidade: null,
            descricao: null,
            diferencial: null
        };
    }
    servico.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, servicoFirebase;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.storage.get('usuario')];
                    case 1:
                        _a.usuario = _b.sent();
                        return [4 /*yield*/, this.servicoProvider.getServico(this.usuario.uid)];
                    case 2:
                        servicoFirebase = (_b.sent()).data();
                        if (servicoFirebase) {
                            this.servico = servicoFirebase;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    servico.prototype.temServicoCadastrado = function () {
        return !!this.servico.uid;
    };
    servico.prototype.presentToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Serviço cadastrado com sucesso!',
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    servico.prototype.openDetail = function () {
        this.navCtrl.parent.select(3);
        this.navCtrl.pop();
        this.presentToast();
    };
    servico.prototype.cadastrar = function () {
        return __awaiter(this, void 0, void 0, function () {
            var load, data, error_1, toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        load = this.loadingCtrl.create();
                        load.present();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        data = __assign({}, this.servico, { uid: this.usuario.uid });
                        return [4 /*yield*/, this.servicoProvider.postServico(data)];
                    case 2:
                        _a.sent();
                        this.openDetail();
                        return [3 /*break*/, 5];
                    case 3:
                        error_1 = _a.sent();
                        toast = this.toastCtrl.create({
                            message: 'Erro ao cadastrar serviço. Tente novamente mais tarde',
                            duration: 3000,
                            position: 'top'
                        });
                        toast.present();
                        return [3 /*break*/, 5];
                    case 4:
                        load.dismiss();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    servico.prototype.alterar = function () {
        return __awaiter(this, void 0, void 0, function () {
            var load, error_2, toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        load = this.loadingCtrl.create();
                        load.present();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        return [4 /*yield*/, this.servicoProvider.updateService(this.usuario.uid, this.servico)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 3:
                        error_2 = _a.sent();
                        console.log(error_2);
                        toast = this.toastCtrl.create({
                            message: 'Erro ao alterar serviço. Tente novamente mais tarde',
                            duration: 3000,
                            position: 'top'
                        });
                        toast.present();
                        return [3 /*break*/, 5];
                    case 4:
                        load.dismiss();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    servico.prototype.salvar = function () {
        return __awaiter(this, void 0, void 0, function () {
            var load, e_1, toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        load = this.loadingCtrl.create();
                        load.present();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 6, 7, 8]);
                        if (!!this.temServicoCadastrado()) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.cadastrar()];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this.alterar()];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5: return [3 /*break*/, 8];
                    case 6:
                        e_1 = _a.sent();
                        toast = this.toastCtrl.create({
                            message: 'Erro ao alterar serviço. Tente novamente mais tarde',
                            duration: 3000,
                            position: 'top'
                        });
                        toast.present();
                        return [3 /*break*/, 8];
                    case 7:
                        load.dismiss();
                        return [7 /*endfinally*/];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    servico = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-servico',template:/*ion-inline-start:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/servico/servico.html"*/'<ion-header>\n\n    <div class="main-header">\n        <ion-navbar color="danger">\n            <ion-title>Cadastrar Serviço</ion-title>\n        </ion-navbar>\n    </div>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-label>Dados fixos</ion-label>\n    <ion-item>\n        <ion-label floating>Nome</ion-label>\n        <ion-input type="text" value="{{usuario.nome}}"></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label floating>Email</ion-label>\n        <ion-input type="text" value="{{usuario.email}}"></ion-input>\n    </ion-item>\n\n    <ion-item>\n        <ion-label floating>Nascimento</ion-label>\n        <ion-input type="text" value="{{usuario.data_nascimento}}"></ion-input>\n    </ion-item>\n    <br>\n\n    <ion-label>Dados do Novo Serviço</ion-label>\n\n    <ion-item>\n        <ion-label color="default">Categoria</ion-label>\n        <ion-select [(ngModel)]="servico.categoria">\n            <ion-option value="carpinteiro">Carpinteiro</ion-option>\n            <ion-option value="pintor">Pintor</ion-option>\n            <ion-option value="eletricista">Eletricista</ion-option>\n            <ion-option value="antenista">Antenista</ion-option>\n            <ion-option value="detetizador">Detetizador</ion-option>\n            <ion-option value="pedreiro">Pedreiro</ion-option>\n            <ion-option value="encanador">Encanador</ion-option>\n            <ion-option value="mecanico">Mecânico</ion-option>\n            <ion-option value="costureiro">Costureiro</ion-option>\n            <ion-option value="alfaiate">Alfaiate</ion-option>\n            <ion-option value="cabeleleiro">Cabeleleiro</ion-option>\n            <ion-option value="psicologo">Psicologo</ion-option>\n            <ion-option value="outros">Outros</ion-option>\n        </ion-select>\n    </ion-item>\n\n    <ion-item>\n        <ion-label>Localidade</ion-label>\n        <ion-input type="text" [(ngModel)]="servico.localidade"></ion-input>\n    </ion-item>\n\n    <ion-item>\n        <ion-label>Diferencial</ion-label>\n        <ion-input type="text" [(ngModel)]="servico.diferencial"></ion-input>\n    </ion-item>\n\n    <ion-item>\n        <ion-label color="default" stacked>Descrição do Serviço</ion-label>\n        <ion-textarea rows="5" maxLength="500" [(ngModel)]="servico.descricao"></ion-textarea>\n    </ion-item>\n    <br>\n    <br>\n    <button ion-button class="addServico" (click)="salvar()">{{ temServicoCadastrado() ? "Alterar" : "Cadastrar" }}</button>\n\n</ion-content>\n'/*ion-inline-end:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/servico/servico.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_servico__["a" /* ServicoProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], servico);
    return servico;
}());

//# sourceMappingURL=servico.js.map

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dataprovider_dataprovider__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__homedetail_homedetail__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_servico__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_usuario__ = __webpack_require__(52);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, dataProvider, servicoProvider, usuarioProvider) {
        this.navCtrl = navCtrl;
        this.dataProvider = dataProvider;
        this.servicoProvider = servicoProvider;
        this.usuarioProvider = usuarioProvider;
        this.chats = [];
        this.servicoProvider.listar();
        console.log('this.chats', this.chats);
        this.layouticon = 'list';
        this.layout = "grid-view";
    }
    HomePage.prototype.delete = function (chip) {
        chip.remove();
    };
    HomePage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var servicos;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.servicoProvider.listar()];
                    case 1:
                        servicos = _a.sent();
                        return [4 /*yield*/, servicos.forEach(function (queryDocument) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    this.chats = [];
                                    queryDocument.forEach(function (servico) { return __awaiter(_this, void 0, void 0, function () {
                                        var servicoObject, usuario;
                                        return __generator(this, function (_a) {
                                            switch (_a.label) {
                                                case 0:
                                                    servicoObject = Object.assign({ uid: null }, servico);
                                                    console.log('s ...', servico, servicoObject);
                                                    return [4 /*yield*/, this.usuarioProvider.getUser(servicoObject.uid)];
                                                case 1:
                                                    usuario = _a.sent();
                                                    this.chats.push({
                                                        usuario: __assign({}, usuario.data(), { face: 'assets/imgs/user1.png' }),
                                                        servico: servico
                                                    });
                                                    return [2 /*return*/];
                                            }
                                        });
                                    }); });
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.openDetail = function (foto, nome, funcao, descricao, telefone, localidade, diferencial) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__homedetail_homedetail__["a" /* HomeDetailPage */], {
            foto: foto,
            nome: nome,
            funcao: funcao,
            descricao: descricao,
            telefone: telefone,
            localidade: localidade,
            diferencial1: diferencial,
        });
    };
    HomePage.prototype.changeicon = function () {
        console.log('change', this.layouticon);
        if (this.layouticon == 'grid') {
            this.layouticon = 'list';
            this.layout = "list-view";
        }
        else {
            this.layouticon = 'grid';
            this.layout = "grid-view";
        }
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/home/home.html"*/'<ion-header>\n  <div class="main-header">\n    <ion-navbar color="danger">\n      <ion-title>Destaques</ion-title>\n        <ion-buttons end>\n       </ion-buttons>\n    </ion-navbar>\n  </div>\n</ion-header>\n\n  <ion-content >\n      <ion-chip #chip1>\n          <ion-label><strong>Dica:</strong> Cadastre seu Serviço na aba "Perfil"</ion-label>\n          <button ion-button clear color="dark" (click)="delete(chip1)">\n            <ion-icon name="close-circle"></ion-icon>\n          </button>\n        </ion-chip>\n    <div class="chat-list-wrap {{layout}}">\n      <ion-list>\n        <ion-item  *ngFor="let chat of chats | slice:0:4 ; index as i" (click)="openDetail(chat.usuario.face,chat.usuario.nome,chat.servico.categoria,chat.servico.descricao,chat.usuario.telefone,chat.servico.localidade,chat.servico.diferencial1)">\n          <img  src="{{chat.usuario.face}}">\n          <div class="chat-list-title">{{chat.usuario.nome}}</div>\n          <div class="chat-list-sub-title">{{chat.servico.categoria}}</div>\n        </ion-item>\n      </ion-list>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_dataprovider_dataprovider__["a" /* DataProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_servico__["a" /* ServicoProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_usuario__["a" /* UsuarioProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(543);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthProvider = /** @class */ (function () {
    function AuthProvider(afAuth) {
        var _this = this;
        this.afAuth = afAuth;
        // Criar usuário
        this.register = function (data) { return _this.afAuth.auth.createUserAndRetrieveDataWithEmailAndPassword(data.email, data.senha); };
        // Login usuário
        this.login = function (data) { return _this.afAuth.auth.signInWithEmailAndPassword(data.email, data.senha); };
    }
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["AngularFireAuth"]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=AuthProvider.js.map

/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(458);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(566);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__ = __webpack_require__(574);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_about_about__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_contact_contact__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_gerenciar_servico_gerenciar_servico__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_splash_screen__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_homedetail_homedetail__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_servico_servico__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__directives_layout_switcher_layout_switcher__ = __webpack_require__(576);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__directives_elastic_header_elastic_header__ = __webpack_require__(577);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_angularfire2__ = __webpack_require__(578);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_angularfire2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_angularfire2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_configs_firebase_config__ = __webpack_require__(579);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_angularfire2_firestore__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_angularfire2_firestore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_18_angularfire2_firestore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_angularfire2_auth__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_19_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_storage__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_dataprovider_dataprovider__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_dataprovider_profile__ = __webpack_require__(580);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__providers_auth_AuthProvider__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_usuario__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__providers_servico__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_usuario_storage__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_login_login_module__ = __webpack_require__(288);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















// Plugin

// PROVIDERS






// PAGES

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_homedetail_homedetail__["a" /* HomeDetailPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_servico_servico__["a" /* servico */],
                __WEBPACK_IMPORTED_MODULE_9__pages_gerenciar_servico_gerenciar_servico__["a" /* GerenciarServicoPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_14__directives_layout_switcher_layout_switcher__["a" /* LayoutSwitcherDirective */],
                __WEBPACK_IMPORTED_MODULE_15__directives_elastic_header_elastic_header__["a" /* ElasticHeaderDirective */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_27__pages_login_login_module__["LoginPageModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/gerenciar-servico/gerenciar-servico.module#GerenciarServicoPageModule', name: 'GerenciarServicoPage', segment: 'gerenciar-servico', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_16_angularfire2__["AngularFireModule"].initializeApp(__WEBPACK_IMPORTED_MODULE_17__providers_configs_firebase_config__["a" /* default */]),
                __WEBPACK_IMPORTED_MODULE_18_angularfire2_firestore__["AngularFirestoreModule"],
                __WEBPACK_IMPORTED_MODULE_19_angularfire2_auth__["AngularFireAuthModule"],
                __WEBPACK_IMPORTED_MODULE_20__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_9__pages_gerenciar_servico_gerenciar_servico__["a" /* GerenciarServicoPage */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_gerenciar_servico_gerenciar_servico__["a" /* GerenciarServicoPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_homedetail_homedetail__["a" /* HomeDetailPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_servico_servico__["a" /* servico */],
                __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_21__providers_dataprovider_dataprovider__["a" /* DataProvider */],
                __WEBPACK_IMPORTED_MODULE_22__providers_dataprovider_profile__["a" /* DataProviderProfile */],
                __WEBPACK_IMPORTED_MODULE_23__providers_auth_AuthProvider__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_24__providers_usuario__["a" /* UsuarioProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers_servico__["a" /* ServicoProvider */],
                __WEBPACK_IMPORTED_MODULE_26__providers_usuario_storage__["a" /* UsuarioStorage */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_firestore__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_firestore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_angularfire2_firestore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


var UsuarioProvider = /** @class */ (function () {
    function UsuarioProvider(angularFirestore) {
        var _this = this;
        this.angularFirestore = angularFirestore;
        // Criar usuário no firestore
        this.postUser = function (data) { return _this.angularFirestore.collection('usuarios').doc(data.uid).set(data); };
        this.getUser = function (uid) { return _this.angularFirestore.collection('usuarios')
            .doc(uid)
            .get()
            .toPromise(); };
        this.update = function (uid, usuario) { return __awaiter(_this, void 0, void 0, function () {
            var dadosUsuarioAtual, usuarioUpdate;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getUser(uid)];
                    case 1:
                        dadosUsuarioAtual = _a.sent();
                        usuarioUpdate = __assign({}, dadosUsuarioAtual.data(), usuario);
                        return [2 /*return*/, this.angularFirestore.collection('usuarios')
                                .doc(uid)
                                .update(usuarioUpdate)];
                }
            });
        }); };
    }
    UsuarioProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_firestore__["AngularFirestore"]])
    ], UsuarioProvider);
    return UsuarioProvider;
}());

//# sourceMappingURL=usuario.js.map

/***/ }),

/***/ 543:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_AuthProvider__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_usuario__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tabs_tabs__ = __webpack_require__(108);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, authProvider, usuarioProvider, storage, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.authProvider = authProvider;
        this.usuarioProvider = usuarioProvider;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.login = true;
        this.register = false;
        this.title = "Login";
        this.loginForm = {
            email: null,
            senha: null
        };
        this.registroForm = {
            nome: null,
            email: null,
            data_nascimento: null,
            senha: null,
            telefone: null,
        };
    }
    LoginPage.prototype.ionViewDidLoad = function () {
    };
    LoginPage.prototype.exibirRegistrar = function () {
        this.title = "Registrar";
        this.login = false;
        this.register = true;
    };
    LoginPage.prototype.exibirLogin = function () {
        this.title = "Login";
        this.login = true;
        this.register = false;
    };
    LoginPage.prototype.logar = function () {
        return __awaiter(this, void 0, void 0, function () {
            var load, responseLogin, usuario, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("logar ....");
                        load = this.loadingCtrl.create();
                        load.present();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, 5, 6]);
                        return [4 /*yield*/, this.authProvider.login(this.loginForm)];
                    case 2:
                        responseLogin = _a.sent();
                        return [4 /*yield*/, this.usuarioProvider.getUser(responseLogin.user.uid)];
                    case 3:
                        usuario = _a.sent();
                        console.log('usuario login', usuario.data(), responseLogin.user.uid);
                        this.storage.set('usuario', usuario.data());
                        console.log(this.storage.get('usuario'));
                        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__tabs_tabs__["a" /* TabsPage */]);
                        return [3 /*break*/, 6];
                    case 4:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [3 /*break*/, 6];
                    case 5:
                        load.dismiss();
                        return [7 /*endfinally*/];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.registrar = function () {
        return __awaiter(this, void 0, void 0, function () {
            var load, response, data, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        load = this.loadingCtrl.create();
                        load.present();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, 5, 6]);
                        return [4 /*yield*/, this.authProvider.register(this.registroForm)];
                    case 2:
                        response = _a.sent();
                        data = __assign({ uid: response.user.uid }, this.registroForm);
                        return [4 /*yield*/, this.usuarioProvider.postUser(data)];
                    case 3:
                        _a.sent();
                        this.storage.set('usuario', data);
                        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__tabs_tabs__["a" /* TabsPage */]);
                        return [3 /*break*/, 6];
                    case 4:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 6];
                    case 5:
                        load.dismiss();
                        return [7 /*endfinally*/];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/login/login.html"*/'<link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">\n<ion-header>\n    <div class="main-header-login">\n        <ion-navbar color="danger">\n          <ion-title> Autonomos</ion-title>\n          </ion-navbar>\n          </div>\n</ion-header>\n\n<ion-content padding>\n    <div *ngIf="login">\n        <ion-item>\n            <ion-label floating>E-mail</ion-label>\n            <ion-input type="email" [(ngModel)]="loginForm.email"></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label floating>Senha</ion-label>\n            <ion-input type="password" [(ngModel)]="loginForm.senha"></ion-input>\n        </ion-item>\n\n        <p class="center">\n            <button ion-button full class="addServico" (click)="logar()">Entrar</button>\n        </p>\n\n        <p class="center" (click)="exibirRegistrar()">Criar um nova conta</p>\n    </div>\n\n    <div *ngIf="register">\n        <!--<ion-item>-->\n            <!--<img class="chat-face">-->\n        <!--</ion-item>-->\n\n        <ion-item>\n            <ion-label floating>Nome</ion-label>\n            <ion-input type="text" [(ngModel)]="registroForm.nome"></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label floating>Email</ion-label>\n            <ion-input type="email" [(ngModel)]="registroForm.email"></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label floating>Nascimento</ion-label>\n            <ion-input type="text" [(ngModel)]="registroForm.data_nascimento"></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label floating>Telefone</ion-label>\n            <ion-input type="text" [(ngModel)]="registroForm.telefone"></ion-input>\n        </ion-item>\n\n        <ion-item>\n            <ion-label floating>Senha</ion-label>\n            <ion-input type="password" [(ngModel)]="registroForm.senha"></ion-input>\n        </ion-item>\n\n        <!--<ion-item>-->\n            <!--<ion-label floating>Confirmar Senha</ion-label>-->\n            <!--<ion-input type="passw" [(ngModel)]="registroForm.confirmar_senha"></ion-input>-->\n        <!--</ion-item>-->\n\n        <p>\n            <button ion-button full class="addServico" (click)="registrar()">Cadastrar</button>\n            \n        </p>\n         <p (click)="exibirLogin()" class="center">Já tenho uma conta</p>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/goliv/Área de Trabalho/pi/piautonomo/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_AuthProvider__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_usuario__["a" /* UsuarioProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 566:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_usuario_storage__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__ = __webpack_require__(108);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, usuarioStorage) {
        var _this = this;
        this.usuarioStorage = usuarioStorage;
        this.rootPage = 'LoginPage';
        platform.ready().then(function () {
            _this.usuarioStorage.getUsuarioLogado()
                .then(function (usuario) {
                _this.rootPage = (usuario) ? __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__["a" /* TabsPage */] : 'LoginPage';
            });
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/goliv/Área de Trabalho/pi/piautonomo/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/goliv/Área de Trabalho/pi/piautonomo/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_4__providers_usuario_storage__["a" /* UsuarioStorage */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 576:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LayoutSwitcherDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LayoutSwitcherDirective = /** @class */ (function () {
    function LayoutSwitcherDirective(element, renderer) {
        this.element = element;
        this.renderer = renderer;
        console.log('Hello LayoutSwitcherDirective Directive');
    }
    LayoutSwitcherDirective.prototype.ngOnInit = function () {
        //  this.element.nativeElement.addEventListener('click', () => {
        //     //  (this.element.nativeElement.firstElementChild)
        //   },false);
    };
    LayoutSwitcherDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[layout-switcher]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Renderer */]])
    ], LayoutSwitcherDirective);
    return LayoutSwitcherDirective;
}());

//# sourceMappingURL=layout-switcher.js.map

/***/ }),

/***/ 577:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ElasticHeaderDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ElasticHeaderDirective = /** @class */ (function () {
    function ElasticHeaderDirective(element, renderer) {
        this.element = element;
        this.renderer = renderer;
    }
    ElasticHeaderDirective.prototype.ngOnInit = function () {
        var _this = this;
        this.scrollerHandle = this.element.nativeElement.getElementsByClassName('scroll-content')[0];
        this.header = this.scrollerHandle.firstElementChild;
        this.headerHeight = this.scrollerHandle.clientHeight;
        this.ticking = false;
        this.renderer.setElementStyle(this.header, 'webkitTransformOrigin', 'center bottom');
        window.addEventListener('resize', function () {
            _this.headerHeight = _this.scrollerHandle.clientHeight;
        }, false);
        this.scrollerHandle.addEventListener('scroll', function () {
            if (!_this.ticking) {
                window.requestAnimationFrame(function () {
                    _this.updateElasticHeader();
                });
            }
            _this.ticking = true;
        });
    };
    ElasticHeaderDirective.prototype.updateElasticHeader = function () {
        this.scrollTop = this.scrollerHandle.scrollTop;
        if (this.scrollTop >= 0) {
            this.translateAmt = this.scrollTop / 2;
            this.scaleAmt = 1;
        }
        else {
            this.translateAmt = 0;
            this.scaleAmt = -this.scrollTop / this.headerHeight + 1;
        }
        this.renderer.setElementStyle(this.header, 'webkitTransform', 'translate3d(0,' + this.translateAmt + 'px,0) scale(' + this.scaleAmt + ',' + this.scaleAmt + ')');
        this.ticking = false;
    };
    ElasticHeaderDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[elastic-header]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Renderer */]])
    ], ElasticHeaderDirective);
    return ElasticHeaderDirective;
}());

//# sourceMappingURL=elastic-header.js.map

/***/ }),

/***/ 579:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// Initialize Firebase
/* harmony default export */ __webpack_exports__["a"] = ({
    apiKey: "AIzaSyB-QIEMp0s7cL6rBpmSm4P4u6Np9lNyjYo",
    authDomain: "piautonomo.firebaseapp.com",
    databaseURL: "https://piautonomo.firebaseio.com",
    projectId: "piautonomo",
    storageBucket: "piautonomo.appspot.com",
    messagingSenderId: "690398783898"
});
//# sourceMappingURL=firebase-config.js.map

/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_firestore__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_firestore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_angularfire2_firestore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__usuario__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__usuario_storage__ = __webpack_require__(83);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var ServicoProvider = /** @class */ (function () {
    function ServicoProvider(angularFirestore, usuarioProvider, usuarioStorage) {
        var _this = this;
        this.angularFirestore = angularFirestore;
        this.usuarioProvider = usuarioProvider;
        this.usuarioStorage = usuarioStorage;
        this.chave = 'servicos';
        // Criar servico no firestore
        this.postServico = function (data) {
            return _this.angularFirestore.collection(_this.chave).doc(data.uid).set(data);
        };
        this.getServico = function (uid) { return _this.angularFirestore.collection(_this.chave)
            .doc(uid)
            .get()
            .toPromise(); };
        this.updateService = function (uid, servico) { return __awaiter(_this, void 0, void 0, function () {
            var dadosServicoAtual, servicoUpdate;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getServico(uid)];
                    case 1:
                        dadosServicoAtual = (_a.sent()).data();
                        servicoUpdate = __assign({}, dadosServicoAtual, servico);
                        return [2 /*return*/, this.angularFirestore.collection(this.chave)
                                .doc(uid)
                                .update(servicoUpdate)];
                }
            });
        }); };
        this.listar = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.angularFirestore.collection(this.chave)
                            .valueChanges()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        }); };
    }
    ServicoProvider.prototype.excluirServico = function (uid) {
        return this.angularFirestore.collection(this.chave).doc(uid).delete();
    };
    ServicoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_firestore__["AngularFirestore"],
            __WEBPACK_IMPORTED_MODULE_2__usuario__["a" /* UsuarioProvider */],
            __WEBPACK_IMPORTED_MODULE_3__usuario_storage__["a" /* UsuarioStorage */]])
    ], ServicoProvider);
    return ServicoProvider;
}());

//# sourceMappingURL=servico.js.map

/***/ }),

/***/ 580:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataProviderProfile; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DataProviderProfile = /** @class */ (function () {
    function DataProviderProfile() {
        this.alldata = [];
        this.alldata = [{
                id: 3,
                nome: 'Gabriel Oliveira Mota',
                email: 'goliveiram4@gmail.com',
                face: 'assets/imgs/user1.png',
                nascimento: '09/07/1998',
            }];
    }
    DataProviderProfile.prototype.getall = function () {
        return this.alldata;
    };
    DataProviderProfile = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], DataProviderProfile);
    return DataProviderProfile;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioStorage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_storage__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UsuarioStorage = /** @class */ (function () {
    function UsuarioStorage(storage) {
        this.storage = storage;
        this.chaveUsuario = 'usuario';
    }
    UsuarioStorage.prototype.getUsuarioLogado = function () {
        return this.storage.get(this.chaveUsuario);
    };
    UsuarioStorage.prototype.updateUsuario = function (usuario) {
        return this.storage.set(this.chaveUsuario, usuario);
    };
    UsuarioStorage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__ionic_storage__["b" /* Storage */]])
    ], UsuarioStorage);
    return UsuarioStorage;
}());

//# sourceMappingURL=usuario-storage.js.map

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DataProvider = /** @class */ (function () {
    function DataProvider() {
        this.alldata = [];
        this.destaque = [];
        this.destaque = [{
                id: 0,
                nome: 'Robson Martins',
                funcao: 'Carpinteiro',
                foto: 'assets/imgs/user1.png',
                preco: '50',
                disconto: '30%',
                descricao: 'Descrição do serviço',
                telefone: '(31) 3333-0927',
                localidade: 'Novo Riacho',
                diferencial1: 'loja fisica',
                diferencial2: 'manutencao',
            }, {
                id: 1,
                nome: 'Alice Vieira',
                funcao: 'Função',
                foto: 'assets/imgs/user2.png',
                preco: '80',
                disconto: '10%',
                descricao: '',
                telefone: '',
                localidade: 'Novo Riacho',
                diferencial1: 'Teste',
                diferencial2: 'Teste 2'
            }, {
                id: 2,
                nome: 'Marcela Santos',
                funcao: 'Função',
                foto: 'assets/imgs/user3.png',
                preco: '120',
                disconto: '20%',
                descricao: '',
                telefone: '',
                localidade: 'Novo Riacho',
                diferencial1: 'Teste',
                diferencial2: 'Teste 2'
            }, {
                id: 3,
                nome: 'Guilherme Soares',
                funcao: 'Função',
                foto: 'assets/imgs/user1.png',
                preco: '30',
                disconto: '15%',
                descricao: '',
                telefone: '',
                localidade: 'Novo Riacho',
                diferencial1: 'Teste',
                diferencial2: 'Teste 2'
            }];
        this.alldata = [{
                id: 0,
                nome: 'Robson Martins',
                funcao: 'Carpinteiro',
                foto: 'assets/imgs/user1.png',
                preco: '50',
                disconto: '30%',
                descricao: 'Descrição do serviço',
                telefone: '(31) 3333-0927',
                localidade: 'Novo Riacho',
                diferencial1: 'loja fisica',
                diferencial2: 'manutencao',
            }, {
                id: 1,
                nome: 'Alice Vieira',
                funcao: 'Função',
                foto: 'assets/imgs/user2.png',
                preco: '80',
                disconto: '10%',
                descricao: '',
                telefone: '',
                localidade: 'Novo Riacho',
                diferencial1: 'Teste',
                diferencial2: 'Teste 2'
            }, {
                id: 2,
                nome: 'Marcela Santos',
                funcao: 'Função',
                foto: 'assets/imgs/user3.png',
                preco: '120',
                disconto: '20%',
                descricao: '',
                telefone: '',
                localidade: 'Novo Riacho',
                diferencial1: 'Teste',
                diferencial2: 'Teste 2'
            }, {
                id: 3,
                nome: 'Guilherme Soares',
                funcao: 'Função',
                foto: 'assets/imgs/user1.png',
                preco: '30',
                disconto: '15%',
                descricao: '',
                telefone: '',
                localidade: 'Novo Riacho',
                diferencial1: 'Teste',
                diferencial2: 'Teste 2'
            }, {
                id: 4,
                nome: 'Daniele Silva',
                funcao: 'Função',
                foto: 'assets/imgs/user2.png',
                preco: '210',
                disconto: '25%',
                descricao: '',
                telefone: '',
                localidade: 'Novo Riacho',
                diferencial1: 'Teste',
                diferencial2: 'Teste 2'
            }];
    }
    DataProvider.prototype.getall = function () {
        return this.alldata;
    };
    DataProvider.prototype.getDestaque = function () {
        return this.destaque;
    };
    DataProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], DataProvider);
    return DataProvider;
}());

//# sourceMappingURL=dataprovider.js.map

/***/ })

},[336]);
//# sourceMappingURL=main.js.map